# Android - Bypass Root Protection

Android application that allows you to learn and pratice how to bypass the root protection.

## Application

You can [download](app/release/app-release.apk) the android application. 

Package source code can be found [here](app/src/main/java/fr/xanhacks/bypassrootprotection).

![application](screenshots/application.png)

## Bypass root

To bypass the root protection we will use a tool named [Frida](https://frida.re/) that allows us to hook functions.

### Frida CLI

**hook_rootcheck.js** : Override the implementation of the function *isDeviceRooted* from the *RootCheck* class to a new function that always returns false.

```javascript
Java.perform(function () {
    var RootCheck = Java.use('fr.xanhacks.bypassrootprotection.RootCheck');
    RootCheck.isDeviceRooted.implementation = function () {
        send("Function is called !");
        return false;
    };
});
```

Run this hook script :

```shell
$ frida -D emulator-5554 -l hook_rootcheck.js -f fr.xanhacks.bypassrootprotection --no-pause
     ____
    / _  |   Frida 14.2.18 - A world-class dynamic instrumentation toolkit
   | (_| |
    > _  |   Commands:
   /_/ |_|       help      -> Displays the help system
   . . . .       object?   -> Display information about 'object'
   . . . .       exit/quit -> Exit
   . . . .
   . . . .   More info at https://frida.re/docs/home/
Spawned `fr.xanhacks.bypassrootprotection`. Resuming main thread!
[Android Emulator 5554::fr.xanhacks.bypassrootprotection]-> message: {'type': 'send', 'payload': 'Function is called !'} data: None
```

### Frida with Python

**bypass_root.py** :

```python
#!/usr/bin/env python3
from sys import stdin
import frida

PACKAGE_NAME = "fr.xanhacks.bypassrootprotection"
EMULATOR_ID = "emulator-5554"

device = frida.get_device(id=EMULATOR_ID)
print("Starting the application :", PACKAGE_NAME)
p1 = device.spawn([PACKAGE_NAME])
print("Attaching to PID :", p1)
process = device.attach(p1)

script = process.create_script("""
Java.perform(function () {
    var RootCheck = Java.use('fr.xanhacks.bypassrootprotection.RootCheck');
    RootCheck.isDeviceRooted.implementation = function () {
        send("Function is called !");
        return false;
    };
});
""")

def on_message(message, data):
    print("[SCRIPT]", message["payload"])

script.on("message", on_message)
script.load()
device.resume(p1)
stdin.read()
```

Execution :

```shell
$ python3 bypass_root.py
Starting the application : fr.xanhacks.bypassrootprotection
Attaching to PID : 13133
[SCRIPT] Function is called !
```

## Made with

- Android Studio
- Java / XML
