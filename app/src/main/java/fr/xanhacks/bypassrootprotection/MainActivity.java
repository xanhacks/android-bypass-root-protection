package fr.xanhacks.bypassrootprotection;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {

    private TextView successTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.successTextView = findViewById(R.id.successTextView);
        this.successTextView.setVisibility(View.GONE);

        this.checkDeviceRoot();
    }

    /**
     * If the device root check returns true, this method will create a
     * popup with a button to leave the application.
     * Otherwise, the success message will be displayed.
     */
    private void checkDeviceRoot() {
        if (RootCheck.isDeviceRooted()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Root device");
            builder.setMessage("You can't use this application as you are on a rooted device !");

            builder.setPositiveButton("Quit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    quitApp();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            this.successTextView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Close this running application.
     */
    private void quitApp() {
        MainActivity.this.finish();
        System.exit(0);
    }

}
